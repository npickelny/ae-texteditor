const SynonymService = (() => {
    const findSynonym = (wordToFind, setSynonym) => {
        fetch(`https://api.datamuse.com/words?ml=${wordToFind}`)
            .then(response => response.json())
            .then(data => setSynonym(data[0].word));
    };

    return {
        findSynonym: (wordToFind, setSynonym) => findSynonym(wordToFind, setSynonym)
    }
})();

export default SynonymService