const ArrayUtils = (() => {
    const handleInsertionOrDeletion = (styleArray, setStyleArray, selectedWordIndex) => {
        let newArr = [];
        if(styleArray.includes(selectedWordIndex)){
            newArr = styleArray.filter((index) => { return index !== selectedWordIndex; });
        } else {
            newArr = styleArray.concat(selectedWordIndex)
        }
        setStyleArray(newArr);
    };

    return {
        handleInsertionOrDeletion: (styleArray, setStyleArray, selectedWordIndex) => handleInsertionOrDeletion(styleArray, setStyleArray, selectedWordIndex)
    }
})();

export default ArrayUtils;