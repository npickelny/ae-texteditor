import React, {Component, useState, useEffect} from 'react';
import './SynonymStyles.css';
import SynonymService from "../utils/SynonymService";

const SynonymComponent = ({wordToFind}) => {

    const [synonym, setSynonym] = useState('');

    useEffect(() => {
        SynonymService.findSynonym(wordToFind, setSynonym)
    }, [wordToFind]);


    return (
        <div className='container'>
            <p> Synonyms </p>
        </div>
    );
};

export default SynonymComponent;