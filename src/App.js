import React, {Component, useState, useEffect} from 'react';
import './App.css';
import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";
import getMockText from './text.service';
import ArrayUtils from "./utils/ArrayUtils";
import SynonymComponent from "./synonym/SynonymComponent";


const App = () => {
    const [textToRender, setTextToRender] = useState('');
    const [selectedWord, setSelectedWord] = useState(null);
    const [selectedWordIndex, setSelectedWordIndex] = useState(null);

    const [boldArrayIndex, setBoldArrayIndex] = useState([]);
    const [italicArrayIndex, setItalicArrayIndex] = useState([]);
    const [underlineArrayIndex, setUnderlineArrayIndex] = useState([]);

    const getText = () => {
        getMockText().then((result) => {
            console.log(result);
            setTextToRender(result)
        });
    };

    useEffect(() => {
        getText();
    }, []);

    const getSelectedWord = (word, index) => {
        console.log("word selected")
        setSelectedWord(word);
        setSelectedWordIndex(index);
    };

    const setStyle = (type) => {
        switch (type) {
            case 'bold':
                ArrayUtils.handleInsertionOrDeletion(boldArrayIndex, setBoldArrayIndex, selectedWordIndex);
                break;
            case 'italic':
                ArrayUtils.handleInsertionOrDeletion(italicArrayIndex, setItalicArrayIndex, selectedWordIndex);
                break;
            case 'underline':
                ArrayUtils.handleInsertionOrDeletion(underlineArrayIndex, setUnderlineArrayIndex, selectedWordIndex);
                break;
        }
    };

    return (
        <div className="App">
            <header>
                <span>Simple Text Editor</span>
            </header>
            <main>
                <ControlPanel setStyleFunc={setStyle}/>
                <FileZone
                    text={textToRender}
                    setSelectedWord={getSelectedWord}
                    stylesArray={[boldArrayIndex, italicArrayIndex, underlineArrayIndex]}
                />
                <SynonymComponent wordToFind={selectedWord}/>
            </main>
        </div>
    );
};

export default App;
