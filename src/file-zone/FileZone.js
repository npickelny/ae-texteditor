import React, { Component } from 'react';
import './FileZone.css';

const Word = ({numb, word, setSelectedWord, stylesArray}) => {
    let [boldArrayIndex, italicArrayIndex, underlineArrayIndex] = [...stylesArray];
    let customStyle = {};

    if(boldArrayIndex.length && boldArrayIndex.includes(numb)){
        customStyle.fontWeight = 'bold'
    }

    if(italicArrayIndex.length && italicArrayIndex.includes(numb)){
        customStyle.fontStyle = 'italic'
    }

    if(underlineArrayIndex.length && underlineArrayIndex.includes(numb)){
        customStyle.textDecoration = 'underline'
    }

    return (
        <span onClick={() => { setSelectedWord(word, numb) }}
              style={customStyle}>
            {` ${word}`}
        </span>
    )
};


const Sentence = ({text, setSelectedWord, stylesArray}) => {
    return(
        <div id="file">
            {
                text.split(' ').map((item, key) => {
                    return <Word key={key} numb={key} word={item} setSelectedWord={setSelectedWord} stylesArray={stylesArray} />
                })
            }
        </div>
    )
}


const FileZone = ({text, setSelectedWord, stylesArray}) => {
    return (
        <div id="file-zone">
            <Sentence text={text} setSelectedWord={setSelectedWord} stylesArray={stylesArray}/>
        </div>
    )
};

export default FileZone;
