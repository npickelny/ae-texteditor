# Simple text editor
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Initial setup
Run `npm install` in order to setup application

## Development server
Run `npm start` for a dev server.



## USAGE
Just double click one word in order to paint it blue. After that just click on one of the three possible styles to apply.

Same behaviour is used to undo the style applied



## Notes
I ran out of time before implementing the synonym service (2,5 hours). i built the structure of the service but couldnt test it. 
The procedure was to find the words painted and replace it in the text using the window.getSelection().toString() method. Also getting the
indexes of said word and replacing it with the slice method


